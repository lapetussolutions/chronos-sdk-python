from setuptools import setup, find_packages

setup(
    name='chronos',

    version='1.0.0',

    description='',
    long_description='',

    url='https://bitbucket.org/lapetussolutions/chronos-sdk-python',

    license='',

    packages=find_packages(),

    install_requires=['requests']
)