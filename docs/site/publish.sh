#!/bin/sh

profile=$1
bucket=$2
shift 2

project_dir="$(git rev-parse --show-toplevel)"
sphinx_dir="$project_dir/docs/sphinx"

while [ "$#" -ge 1 ];
do
    dir="$1"
    shift
    echo "Uploading docs for $dir"
    aws s3 sync "$sphinx_dir/site/build/$dir" "s3://$bucket/$dir" --profile "$profile" --acl public-read
done