#!/bin/sh

initial_dir=$(pwd)
project_dir="$(git rev-parse --show-toplevel)"
sphinx_dir="$project_dir/docs/sphinx"
initial_git_head=$(git rev-parse --abbrev-ref HEAD)

checkout() {
    cd "$project_dir"
    git checkout "$1" &> /dev/null
    python3 -m pip uninstall -y chronos
    python3 -m pip install -e .
    echo "Checked out $1"
}

while [ "$#" -ge 1 ];
do
    tag="$1"

    checkout "$tag"

    cd "$sphinx_dir"
    echo "Building $1..."
    rm -rf build "site/build/$1"
    make html
    cp -r build/html "site/build/$1"
    echo "Built $1 at $sphinx_dir/"

    shift
done


checkout "$initial_git_head"
cd "$initial_dir"