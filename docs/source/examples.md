## Examples

### Create a Chronos instance

```python
from chronos import Chronos

chronos = Chronos({
    "apiroot": "https://chronos.lapetussolutions.com/v1",
    "orgid": "[orgid]",
    "appid": "[appid]",
    "apikey": "[apikey]"
})
```

### Chronos Methods

#### Make a request

```python

request_options = {
    # http method
    "method": "get",

    # post data
    "data": {},

    # query string parameters
    "params": {},

    # headers
    "headers": {},
}

res = chronos.request(request_options)

```

Any additional key/values in the request options dict will be directly be passed as keyword arguments to `requests.request` (see [requests](http://docs.python-requests.org/en/master/api/#requests.request) documentation for the full list)

The return value for any request method will be a [requests.Response](http://docs.python-requests.org/en/master/api/#requests.Response)

### User Methods

#### Create a User

```python
props = {
    "dob": "YYYYMMDD",
    "gender": "[male|female]",
    "email": "some-email@domain.com",
    "name": "John Doe",
    "postcode": "12345",
    "loc": "USA",
    "meta": {
        "some_key": "some_val"
    }
}

user = chronos.create_user(props)
print("created: " + user.is_created())
# True
```

#### Access an Existing User

```python
user = chronos.user({
    "id": "[userid]",
    "auth_token": "[token]"
})
```

#### User Data Object

##### Get

```python
print(user.get_user().json())
"""
{
    "id": "[userid]",
    "dob": "1980-01-01T00:00:00+0000",
    ...
}
"""
```

##### Update

```python
props = {
    "dob": "19800101",
    "meta": {
        "some_other_key": "some_other_val",
        "nested": {
            "keys": {
                "work": "too"
            },
        },
        "so.do.keys.like": "this"
    }
}

user.update_user(props)
```

#### User Meta Object

The following two methods are for the meta object, a subset of the data object

##### Get

```python
print(user.get_meta().json())
"""
{
    ...
    "somekey": "someval",
    "someotherkey": "someotherval",
    "nested": {
        "keys": {
            "work": "too"
        }
    },
    "so": {
        "do": {
            "keys": {
                "like": "this"
            }
        }
    }
}
"""
```

##### Update

```python
meta = {
    "more.nested.keys": ["more", "values"]
}

user.update_meta(meta)
```

#### Upload an Image

Note: This will overwrite whatever image is currently uploaded to the user, regardless of whether the request results in an error.

```python
filename = "/path/to/image.jpg"
print(user.upload_image(filename).json())

"""
{
    'id': '[userid]',
    'face_href': '[apiroot]/o/[orgid]/a/[appid]/u/[userid]/image/face',
    'registered_href': '[apiroot]/o/[orgid]/a/[appid]/u/[userid]/image/face/registered',
    'estimations_href': '[apiroot]/o/[orgid]/a/[appid]/u/[userid]/image/face/estimations',
    'aged_href': '[apiroot]/o/[orgid]/a/[appid]/u/[userid]/image/face/aged'
}
"""
```

The criteria for image quality can be found [here](../../working-with-images.html)

#### ID Images

##### Upload

You may upload 1 image per "side" of the ID (currently the "front" and "back" sides are supported). Uploading an image to a side will overwrite the image for that side.

```python

user.upload_id_image(front_filename, side="front")
user.upload_id_image(back_filename, side="back")

```

##### Get Analysis

After uploading at least the "front" image of the ID, you can use the `get_id_image_analysis` method to get the analysis of the image. This call is asynchronous, so you will need to call it once, and then after 10 seconds call at whichever interval you want (1 second in this example) until the status is no longer "In Progress".

```python
import time

analysis = instance.get_id_image_analysis()
time.sleep(10)

for i in range(30):
    analysis = instance.get_id_image_analysis().json()
    if analysis["status"] != "In Progress":
        break
    time.sleep(1)

print(analysis)
```

##### Delete

You can delete the user's ID images and analysis by using the `delete_id_images` method:

```python

user.delete_id_images()

```



### Handling Exceptions

#### Requests

The `chronos.exceptions.RequestException` exception will be raised if there is a connection error, a timeout error, or if the response status is 4XX or 5XX

It is recommended that you account for exceptions for all requests, especially if it is a client-facing project

```python
from chronos import Chronos, exceptions

chronos = Chronos({
    "apiroot": "https://chronos.lapetussolutions.com/v1",
    "orgid": "[orgid]",
    "appid": "[appid]",
    "apikey": "[apikey]"
})

try:
    chronos.get('/some/invalid/request')
except exceptions.RequestException as e:
    response = e.args[0]["response"]
    errors = e.args[0]["errors"]
    print(response.text)
    """{"status":404,"code":404,"message":"Unknown Request","developerMessage":"The requested URI was not found."}"""
    print(response.status_code)
    """404"""
    print(errors)
    """['The request failed. Check the response for more details']"""
```

#### User Exceptions

##### Image Upload Errors

```python

user = chronos.create_user()
try:
    user.upload_image('/mouth/open/image.jpg')
except exceptions.RequestException as e:
    response = e.args[0]["response"]
    errors = e.args[0]["errors"]
    print(response.text)
    """{"status":400,"code":400,"message":"Image failed quality checks","developerMessage":"Check 'details' for the reason the image failed and try again.","details":{"id":"","image":{"contrast":...,"focus":...},"face":{...},"closedeyes":{...}}}"""
    print(response.status_code)
    """400"""
    print(errors)
    """['You might be smiling or your mouth may be open. Please try again with a neutral expression and a closed mouth.']"""
```

##### User Not Created

A user is "created" if the `id` and `auth_token` properties are set. You can check the created status by using `.isCreated()`

Any user method that makes a request (with the exception of `.create`) will raise a `chronos.exceptions.UserNotCreated` exception if the user is not "created". Check the `Raises` section of a method in the `chronos.User` documentation for more details

```python

# user properties not passed
user = chronos.user()
user.get('/')

"""
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "[chronos path]\user.py", line 127, in decorated_function
    raise UserNotCreated("User must be created to use this method")
chronos.exceptions.user.UserNotCreated: User must be created to use this method
"""

```

##### User Already Created

If `.create` is called and the user is already "created", it will raise a `chronos.exceptions.UserAlreadyCreated` exception

```python

user = chronos.create_user()
user.create()

"""
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "[chronos path]\user.py", line 129, in decorated_function
    raise UserAlreadyCreated("User must not be created to use this method")
chronos.exceptions.user.UserAlreadyCreated: User must not be created to use this method
"""

```

### Request Modifiers

#### Overview

Modifiers are designed to make it easier to define and overwrite default request options (e.g. query string parameters, post data, headers, etc.)

The request options dict will pass through the following sequence:

```python
from chronos import Chronos
instance = Chronos({})
request_options = {
    "params": {},
    "data": {},
    "headers": {},
}
options = {
    "modifiers": {
        "pre": [],
        "post": [],
    },
    "override_modifiers": False
}
instance.request(request_options, options)
###
the request_options parameter will be passed through every modifier in the following arrays:
instance.modifiers["pre"]
instance.modifiers["post"]
options["modifiers"]["pre"]
options["modifiers"]["post"]

if options["override_modifiers"] is True, the request_options parameter will only be passed through the modifiers in options["modifiers"]
###
```

#### Define your own modifier

Modifiers can be defined in two ways. Here are two examples of how you could define a modifier to add headers to a request:

```python
from chronos import RequestModifier

additional_headers = {
    "Some-Header": "Some-Value",
    "Another-Header": "Another-Value",
}

class SetAdditionalHeadersModifier(RequestModifier):
    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        # custom code could go here

    def request_options(self, instance, request_options, options=None):
        super().request_options(instance, request_options, options)
        if "headers" not in request_options:
            request_options["headers"] = {}
        request_options["headers"].update(additional_headers)

def set_additional_headers_modifier(instance, request_options, options=None):
    if "headers" not in request_options:
        request_options["headers"] = {}
    request_options["headers"].update(additional_headers)
```

#### Add a default modifier

To have a modifier be applied to every request in a `Chronos` or `User` instance, add the modifier to the appropriate list in the `modifiers` property

```python

chronos.modifiers["pre"].append(SetAdditionalHeadersModifier())
user.modifiers["pre"].append(SetAdditionalHeadersModifier())

# or

chronos.modifiers["pre"].append(set_additional_headers_modifier)
user.modifiers["pre"].append(set_additional_headers_modifier)

```