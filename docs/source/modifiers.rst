chronos.modifiers
================================================

AuthModifier
----------------------

.. autoclass:: chronos.modifiers.AuthModifier
   :members:

CompactModifier
----------------------

.. autoclass:: chronos.modifiers.CompactModifier
   :members:

ContentLengthModifier
----------------------

.. autoclass:: chronos.modifiers.ContentLengthModifier
   :members:

ContentTypeModifier
----------------------

.. autoclass:: chronos.modifiers.ContentTypeModifier
   :members:

UserAgentModifier
----------------------

.. autoclass:: chronos.modifiers.UserAgentModifier
   :members:

AuthDefaultModifier
----------------------

.. autoclass:: chronos.modifiers.auth.default.AuthDefaultModifier
   :members:

Helpers
------------------------------------------------

apply_request_modifier
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: chronos.apply_request_modifier

apply_request_modifiers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: chronos.apply_request_modifiers
