|ProjectName| for Chronos |ProjectVersion|
================================================

.. toctree::
   :maxdepth: 2

   installation
   examples
   documentation

* :ref:`genindex`
* :ref:`search`