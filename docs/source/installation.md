## Installation

### With Pip (recommended)

Dependencies:

- Python 3
- Pip
- Git

To install the Chronos python package onto your system, run:

```sh
python -m pip install git+https://bitbucket.org/lapetussolutions/chronos-sdk-python.git@v1.0.0
```

Then import the SDK at the top of your python file:

```python
from chronos import Chronos
```

### From Source

Dependencies

- Python 3
- Python [requests](http://docs.python-requests.org/en/master/) package

#### Download

- Clone with Git:

    ```sh
    git clone https://bitbucket.org/lapetussolutions/chronos-sdk-python.git [installdirectory]
    cd [installdirectory]
    git checkout tags/v1.0.0
    python -m pip install -r requirements.txt
    cd ..
    ```
- or, [download tag directly](https://bitbucket.org/lapetussolutions/chronos-sdk-python/downloads/?tab=tags)

#### Import package from local directory

Then import the SDK at the top of your python file:

```python
from [installdirectory].chronos import Chronos
```
