chronos.RequestModifier
================================================

.. autoclass:: chronos.RequestModifier
   :members: request_options
