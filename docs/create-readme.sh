#!/bin/sh

project_dir="$(git rev-parse --show-toplevel)"
cd "$project_dir"

echo "# Chronos SDK - Python

An SDK for interacting with Chronos API v1.0.0 in Python 3
" > README.md

cat docs/sphinx/source/installation.md >> README.md
echo "" >> README.md
cat docs/sphinx/source/examples.md >> README.md
