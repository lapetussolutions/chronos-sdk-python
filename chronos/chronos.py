from urllib.parse import urlencode, quote_plus
import requests, json, copy, sys

from .user import User
from .modifiers import *
from .request_modifier import RequestModifier, apply_request_modifier, apply_request_modifiers
from .exceptions import RequestException, InvalidResponseFormat

def url_join(urls):
    return "/".join(map(lambda url: url.strip("/"), urls))

class Chronos:
    """Class for interacting with Chronos API v1.0.0

    Args:
        props (dict): Properties dictionary containing Chronos API endpoint and credentials
        config (dict, optional): Configurization dict:

    Attributes:
        version (str): Chronos API Version
        props (dict): Properties dictionary containing Chronos API endpoint and credentials

            - `apiroot` (str): Chronos API root
            - `orgid` (str): Chronos Organization ID
            - `apikey` (str): Chronos Organization API Key
            - `appid` (str): Chronos App ID
        config (dict): Configurization dict:

            - `compact_mode` (bool, optional): If `True`, all requests will have the `"compact=true"` query string parameter added. Defaults to `True`
            - `user_agent` (str, optional): User agent to be sent in each request. Defaults to "chronos-sdk-python/1.0.0 (Python [python version]; requests [requests version])"
        auth ([chronos.RequestModifier, function, str]): Authentication request modifier. Defaults to `"default"`
        modifiers (dict): Dictionary of default request modifiers

            - `pre` (list): List of request modifiers to be applied to all requests
            - `post` (list): List of request modifiers to be applied to all requests after the `pre` list

    """

    def __init__(self, props, config=None):
        self.version = "1.0.0"

        self.props = props
        for key in ["apiroot", "orgid", "apikey", "appid"]:
            if not (key in self.props and type(self.props[key]) is str):
                self.props[key] = ""

        if self.props["apiroot"][-1:] == "/":
            self.props["apiroot"] = self.props["apiroot"][:-1]

        if type(config) is not dict:
            config = {}
        self.config = config
        if "compact_mode" not in self.config:
            self.config["compact_mode"] = True
        if "user_agent" not in self.config:
            self.config["user_agent"] = "chronos-sdk-python/{sdk_version} (Python {python_version}; requests {requests_version})".format(sdk_version=self.version, python_version=".".join(map(str, sys.version_info[:3])), requests_version=requests.__version__)

        self.set_auth_method("default")

        self.modifiers = {
            "pre": [
                AuthModifier({ "instance": self }),
                UserAgentModifier()
            ],
            "post": [
                ContentTypeModifier(),
                ContentLengthModifier(),
                AcceptModifier(),
                CompactModifier(),
                self.url
            ],
        }

    def set_auth_method(self, method="default", options=None):
        """Sets the authentication request modifier

        Args:
            method ([chronos.RequestModifier, function, str]): Authentication request modifier. Defaults to `"default"`
            options (dict, optional): Options to be passed to the modifier. Only used if `method` is a `chronos.RequestModifier` or `str`

        Examples:
            >>> from chronos import Chronos
            >>> instance = Chronos({})
            >>> instance.set_auth_method("default", {})

        """

        if type(method) is str:
            method = auth.auth_methods[method]
        if issubclass(method, RequestModifier):
            self.auth = method(options)
        elif callable(method):
            self.auth = method
        else:
            raise TypeError("The `method` property must be a chronos.RequestModifier, function, or str")

    def combine_modifiers(modifier_objects, order=["pre", "post"]):
        """Combines a list of modifier objects into a list of modifiers

        Args:
            modifier_objects (list of dict): List of modifier objects
            order (list of str): Order of modifier list keys

        Returns:
            list: List of modifiers

        Examples:
            >>> from chronos import Chronos

            >>> def modifier_A(request_options, options=None):
            >>>     pass
            >>> def modifier_B(request_options, options=None):
            >>>     pass
            >>> def modifier_C(request_options, options=None):
            >>>     pass
            >>> def modifier_D(request_options, options=None):
            >>>     pass

            >>> modifiers = {
            >>>     "pre": [modifier_A],
            >>>     "post": [modifier_C],
            >>> }

            >>> additional_modifiers = {
            >>>     "pre": [modifier_B],
            >>>     "post": [modifier_D],
            >>> }

            >>> print(Chronos.combine_modifiers([modifiers, additional_modifiers]))
            [<function modifier_A at 0x...>, <function modifier_B at 0x...>, <function modifier_C at 0x...>, <function modifier_D at 0x...>]

        """

        modifiers = []
        for key in order:
            for modifier_object in modifier_objects:
                try:
                    modifiers.extend(modifier_object[key])
                except Exception:
                    pass
        return modifiers

    def org_href(self):
        """Path of the Chronos orginization

        Returns:
            str: Organization path

        Examples:
            >>> from chronos import Chronos
            >>> instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>> })
            >>> print(instance.org_href())
            /o/[orgid]

        """
        return "/o/" + self.props["orgid"]

    def app_href(self):
        """Relative path of the Chronos app

        Returns:
            str: App path added to the organization path

        Examples:
            >>> from chronos import Chronos
            >>> instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>> })
            >>> print(instance.org_href())
            /o/[orgid]/a/[appid]

        """
        return self.org_href() + "/a/" + self.props["appid"]

    def url(self, instance, request_options, options=None):
        """Request modifier to create the base url for the orgid and appid in the `props` property

        Args:
            instance (chronos.Chronos): Chronos instance. This argument is ignored
            request_options (dict): Request options dictionary
                - `url` (str, optional): Base url for a request. Defaults to the instance's `app_href()`
                - `path` (str, optional): Path to be added to the base url
            options (dict, optional): Options dictionary. This argument is ignored

        Examples:
            >>> from chronos import Chronos, apply_request_modifier
            >>> instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>> })

            >>> request_options = {}
            >>> apply_request_modifier(instance.url, instance, request_options)
            >>> print(request_options)
            {'url': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]'}

            >>> request_options = {
            >>>     "path": "/users"
            >>> }
            >>> apply_request_modifier(instance.url, instance, request_options)
            >>> print(request_options)
            {'url': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/users'}

            >>> request_options = {
            >>>     "url": "https://some_url",
            >>> }
            >>> apply_request_modifier(instance.url, instance, request_options)
            >>> print(request_options)
            {'url': 'https://some_url'}

            >>> request_options = {
            >>>     "url": "https://some_url",
            >>>     "path": "/some_path",
            >>> }
            >>> apply_request_modifier(instance.url, instance, request_options)
            >>> print(request_options)
            {'url': 'https://some_url/some_path'}

        """

        if request_options is None:
            request_options = {}
        if "url" not in request_options:
            request_options["url"] = url_join([self.props["apiroot"], self.app_href()])
        if not ("path" in request_options and type(request_options["path"]) is str and len(request_options["path"])):
            return
        request_options["url"] = url_join([request_options["url"], request_options["path"]])
        request_options.pop("path", None)

    def request_options(self, request_options, options=None):
        """Creates full request options dictionary

        Args:
            request_options (dict): Request options dictionary
                - `method` (str, optional): Request method. Defaults to `"get"`
                - `url` (str, optional): Base url for a request. Defaults to the instance's `app_href()`
                - `path` (str, optional): Path to be added to the base url
                - `headers` (dict, optional): Request headers
                - `params` (dict, optional): Request query string
                - `data` (any, optional): Request data
            options (dict): Options dictionary. This will also be passed to the modifiers
                - `modifiers` (dict, optional): Modifiers to be combined with the instance's default modifiers and applied to the request options
                - `override_modifiers` (bool, optional): If `True`, the instance's default modifiers will not be applied to the request options

        Examples:
            >>> from chronos import Chronos
            >>> instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> request_options = {
            >>>     "method": "post",
            >>>     "path": "/users",
            >>>     "data": {
            >>>         "dob": "19900101",
            >>>     }
            >>> }
            >>> instance.request_options(request_options, {})
            >>> print(request_options)
            {'method': 'post', 'data': {'dob': '19900101'}, 'headers': {'Authorization': 'apikey [apikey]', 'User-Agent': 'chronos-sdk-python/1.0.0 (Python ...; requests ...)', 'Content-Type': 'application/json', 'Content-Length': '19'}, 'params': {'compact': 'true'}, 'url': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/users'}

            >>> def set_additional_headers_and_params(instance, request_options, options=None):
            >>>     if "headers" not in request_options:
            >>>         request_options["headers"] = {}
            >>>     if "params" not in request_options:
            >>>         request_options["params"] = {}
            >>>     request_options["headers"].update({
            >>>         "Some-Header": "Some-Value",
            >>>     })
            >>>     request_options["params"].update({
            >>>         "Some-Param": "Some-Value",
            >>>     })

            >>> request_options = {
            >>>     "method": "post",
            >>>     "path": "/users",
            >>>     "data": {
            >>>         "dob": "19900101",
            >>>     }
            >>> }
            >>> instance.request_options(request_options, {
            >>>     "modifiers": {
            >>>         "post": [set_additional_headers_and_params]
            >>>     }
            >>> })
            >>> print(request_options)
            {'method': 'post', 'data': {'dob': '19900101'}, 'headers': {'Authorization': 'apikey [apikey]', 'User-Agent': 'chronos-sdk-python/1.0.0 (Python 3.x.x; requests x.x.x)', 'Content-Type': 'application/json', 'Content-Length': '19', 'Some-Header': 'Some-Value'}, 'params': {'compact': 'true', 'Some-Param': 'Some-Value'}, 'url': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/users'}

        """

        if options is None:
            options = {}

        if "method" not in request_options:
            request_options["method"] = "get"
        if "headers" not in request_options:
            request_options["headers"] = {}
        if "params" not in request_options:
            request_options["params"] = {}
        if "data" not in request_options:
            request_options["data"] = {}

        base_modifiers = self.modifiers
        if "override_modifiers" in options and options["override_modifiers"]:
            base_modifiers = {}

        modifier_objects = [base_modifiers]
        if "modifiers" in options:
            modifier_objects.append(options["modifiers"])
        modifiers = Chronos.combine_modifiers(modifier_objects)

        apply_request_modifiers(modifiers, self, request_options, options)

    def normalize_request_options(request_options):
        """Makes the request options a dictionary if a str is passed

        Args:
            request_options ([str, dict]): Request options dictionary

        Examples:
            >>> from chronos import Chronos

            >>> request_options = Chronos.normalize_request_options({})
            >>> print(request_options)
            {}
            >>> request_options = Chronos.normalize_request_options("/some_path")
            >>> print(request_options)
            {'path': '/some_path'}

        """
        if type(request_options) is str:
            request_options = { "path": request_options }
        return request_options

    def request(self, request_options, options=None):
        """Makes a request

        Args:
            request_options (dict): Request options dictionary
                - `method` (str, optional): Request method. Defaults to "get"
                - `url` (str, optional): Base url for a request. Defaults to the instance's `app_href()`
                - `path` (str, optional): Path to be added to the base url
                - `headers` (dict, optional): Request headers
                - `params` (dict, optional): Request query string
                - `data` (any, optional): Request data
                - Any other parameters found in the requests documentation (http://docs.python-requests.org/en/master/api/#main-interface) may be used
            options (dict): Options dictionary. This will also be passed to the modifiers
                - `accept` (bool, optional): If `True`, a chronos.exceptions.RequestException exception will be raised if the response text is not valid JSON
                - `modifiers` (dict, optional): Modifiers to be combined with the instance's default modifiers and applied to the request options
                - `override_modifiers` (bool, optional): If `True`, the instance's default modifiers will not be applied to the request options

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> # This will create a user by manually making a request. Using the `create_user` method is the recommended way to create a user.
            >>> request_options = {
            >>>     "method": "post",
            >>>     "path": "/users",
            >>>     "data": {
            >>>         "dob": "19900101",
            >>>     }
            >>> }
            >>> request = instance.request(request_options, {})
            >>> print(request)
            <Response [201]>
            >>> user_props = request.json()
            >>> print(request.json())
            {'id': '[id]', 'orgid': '[orgid]', 'appid': '[appid]', 'href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]', ..., 'dob': '1990-01-01T00:00:00+0000', ..., 'auth_token': '[auth_token]', ...}

        """

        request_options = Chronos.normalize_request_options(request_options)
        if options is None:
            options = {}

        self.request_options(request_options, options)

        accept = get_header("Accept", request_options)
        if not accept:
            accept = "application/json"

        if "data" in request_options and type(request_options["data"]) is dict:
            request_options["data"] = json.dumps(request_options["data"])

        method = request_options["method"]
        request_options.pop("method", None)
        url = request_options["url"]
        request_options.pop("url", None)

        try:
            response = requests.request(method, url, **request_options)
        except requests.exceptions.ConnectionError:
            raise RequestException(response, "Connection Error")
        except requests.exceptions.Timeout:
            raise RequestException(response, "Timeout Error")
        except requests.exceptions.RequestException:
            raise RequestException(response, "The request failed for an unknown reason. Check the response for more details")

        try:
            result = response.json()
            if "errors" in result and result["errors"] and len(result["errors"]):
                raise RequestException(response, result["errors"])
        except json.decoder.JSONDecodeError:
            if accept == "application/json":
                raise InvalidResponseFormat(expected=accept, response=response, errors=["Response format should be %s" % accept])

        if response.status_code >= 400 and response.status_code <= 599:
            raise RequestException(response, "The request failed for an unknown reason. Check the response for more details")

        return response

    def get(self, request_options, **kwargs):
        """Makes a request using the GET method. See `request` method for more details

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        """

        request_options = Chronos.normalize_request_options(request_options)
        request_options["method"] = "get"
        return self.request(request_options, **kwargs)

    def post(self, request_options, **kwargs):
        """Makes a request using the POST method. See `request` method for more details

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        """
        request_options = Chronos.normalize_request_options(request_options)
        request_options["method"] = "post"
        return self.request(request_options, **kwargs)

    def put(self, request_options, **kwargs):
        """Makes a request using the PUT method. See `request` method for more details

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        """

        request_options = Chronos.normalize_request_options(request_options)
        request_options["method"] = "put"
        return self.request(request_options, **kwargs)

    def delete(self, request_options, **kwargs):
        """Makes a request using the DELETE method. See `request` method for more details

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        """

        request_options = Chronos.normalize_request_options(request_options)
        request_options["method"] = "delete"
        return self.request(request_options, **kwargs)

    def user(self, *args, **kwargs):
        """Creates an instance of the User class

        Args:
            **args (any): Unnamed args to pass to the User
            **kwargs (any): Named args to pass to the User

        Returns:
            chronos.User: Chronos user instance

        Examples:
            >>> from chronos import Chronos, User
            >>> instance = Chronos({})

            >>> user = instance.user({})
            >>> print(user.is_created())
            False

            >>> user = instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> print(user.is_created())
            True

        """

        return User(self, *args, **kwargs)

    def create_user(self, data=None, *args, **kwargs):
        """Creates an instance of the User class, and saves the user to Chronos

        Args:
            data (dict, optional): User data to be sent in the create request
            **args (any): Unnamed args to pass to the User
            **kwargs (any): Named args to pass to the User

        Returns:
            chronos.User: Chronos user instance

        Raises:
            chronos.exceptions.RequestException: There was an error while creating the user

        """
        user = self.user(*args, *kwargs)
        if not user.is_created():
            user.create(data)
        return user
