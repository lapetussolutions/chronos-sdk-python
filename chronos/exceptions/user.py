from .request_exception import RequestException

class UserAlreadyCreated(Exception):
    pass

class UserNotCreated(Exception):
    pass

class ImageValidationFailed(RequestException):
    pass

class ImageAlreadyUploaded(RequestException):
    pass
