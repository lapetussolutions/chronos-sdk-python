class RequestException(Exception):
    """Exception used for request errors"""

    def __init__(self, response, errors=None):
        obj = {
            "response": response,
            "errors": []
        }
        if type(errors) is list:
            obj["errors"] = errors
        elif errors is not None:
            obj["errors"] = [errors]
        super(RequestException, self).__init__(obj)
