from .request_exception import RequestException

class InvalidResponseFormat(RequestException):
    """Exception used for indicating an invalid response format"""

    def __init__(self, expected, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.args[0]["expected"] = expected
