class RequestModifier:
    """Base class for making custom request modifiers

    Args:
        options (dict): Request modifier options

    """

    def __init__(self, options=None):
        if options is None:
            options = {}
        self.options = options

    def request_options(self, instance, request_options, options=None):
        """Method to be extended to modify requests
        
        Args:
            instance (any): Object to be used by the modifier
            request_options (dict): Request options dict to be modified
            options (dict): Options to be used by the modifier

        """
        pass
    

def apply_request_modifier(modifier, instance, request_options, options=None):
    """Applies a modifier to a request options dictionary

    Args:
        modifier ([chronos.RequestModifier, function]): Authentication request modifier
        instance ([chronos.Chronos, chronos.User, object]): Instance to pass to the modifier
        request_options (dict): Request options dictionary
        options (dict, optional): Additional options to be passed to the modifier

    Examples:
        >>> from chronos import Chronos, apply_request_modifier
        >>> instance = Chronos({})

        >>> def set_additional_headers_modifier(instance, request_options, options=None):
        >>>     if "headers" not in request_options:
        >>>         request_options["headers"] = {}
        >>>     request_options["headers"].update({
        >>>         "Some-Header": "Some-Value",
        >>>         "Another-Header": "Another-Value",
        >>>     })

        >>> request_options = {}
        >>> apply_request_modifier(set_additional_headers_modifier, instance, request_options)
        >>> print(request_options)
        {'headers': {'Some-Header': 'Some-Value', 'Another-Header': 'Another-Value'}}

    """

    if callable(modifier):
        modifier(instance, request_options, options)
        return
    try:
        fn = getattr(modifier, "request_options")
        fn(instance, request_options, options)
    except AttributeError:
        pass

def apply_request_modifiers(modifiers, instance, request_options, options=None):
    """Applies a list of modifiers to a request options dictionary

    Args:
        modifiers (list of [chronos.RequestModifier, function]): Authentication request modifiers
        instance ([chronos.Chronos, chronos.User, object]): Instance to pass to the modifier
        request_options (dict): Request options dictionary
        options (dict, optional): Additional options to be passed to the modifier

    Examples:
        >>> from chronos import Chronos, apply_request_modifier
        >>> instance = Chronos({})

        >>> def set_additional_headers_modifier(instance, request_options, options=None):
        >>>     if "headers" not in request_options:
        >>>         request_options["headers"] = {}
        >>>     request_options["headers"].update({
        >>>         "Some-Header": "Some-Value",
        >>>         "Another-Header": "Another-Value",
        >>>     })

        >>> request_options = {}
        >>> apply_request_modifiers([set_additional_headers_modifier], instance, request_options)
        >>> print(request_options)
        {'headers': {'Some-Header': 'Some-Value', 'Another-Header': 'Another-Value'}}

    """

    for modifier in modifiers:
        apply_request_modifier(modifier, instance, request_options, options)
