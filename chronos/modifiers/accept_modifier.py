from .helpers import set_header
from ..request_modifier import RequestModifier

class AcceptModifier(RequestModifier):
    """Request modifier to set the `Accept` header"""

    def request_options(self, instance, request_options, options=None):
        """Sets the `Accept` header to "application/json" if the `Accept` header is not already set"""

        set_header("Accept", "application/json", request_options)
