from ..request_modifier import RequestModifier, apply_request_modifier

class AuthModifier(RequestModifier):
    """Request modifier wrapper for applying the `auth` property request modifier of the instance"""

    def request_options(self, instance, request_options, options=None):
        """Applies the instance's `auth` modifier"""

        apply_request_modifier(self.options["instance"].auth, self.options["instance"], request_options, options)
