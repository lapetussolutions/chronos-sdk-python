def normalize_header_keys(request_options):
    if "headers" not in request_options:
        return {}
    keys = map(lambda key: key.lower(), request_options["headers"].keys())
    values = request_options["headers"].values()
    return dict(zip(keys, values))

def has_header(header, request_options):
    headers = normalize_header_keys(request_options)
    return header.lower() in headers

def get_header(header, request_options, default=None):
    headers = normalize_header_keys(request_options)
    header = header.lower()
    return headers[header] if header in headers else default

def set_header(header, value, request_options, force=False):
    headers = normalize_header_keys(request_options)
    if header.lower() in headers and not force:
        return
    if "headers" not in request_options:
        request_options["headers"] = {}
    request_options["headers"][header] = value
