from .helpers import set_header
from ..request_modifier import RequestModifier

class ContentTypeModifier(RequestModifier):
    """Request modifier to set the `Content-Type` header"""

    def request_options(self, instance, request_options, options=None):
        """Sets the `Content-Type` header to "application/json" if the `Content-Type` header is not already set"""

        set_header("Content-Type", "application/json", request_options)
