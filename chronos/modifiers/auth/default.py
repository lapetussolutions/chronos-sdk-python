from ...request_modifier import RequestModifier

class AuthDefaultModifier(RequestModifier):
    """Request modifier for setting the `Authorization` header in the following format: `[authorization_type] [token]`

    Args:
        options (dict): Request modifier options

            - `props_key` (string): Name of the instance's props array property
            - `token_key` (string): Key in which the token is stored in the instance's prop array
            - `authorization_type` (string): Prefix of the `Authorization` header value. Defaults to the value of `token_key`

    """

    def __init__(self, options=None):
        super().__init__(options)
        if "props_key" not in self.options:
            self.options["props_key"] = "props"
        if "token_key" not in self.options:
            self.options["token_key"] = "apikey"
        if "authorization_type" not in self.options:
            self.options["authorization_type"] = self.options["token_key"]

    def request_options(self, instance, request_options, options=None):
        """Sets the Authorization header. The options define which properties are pulled from the instance"""

        super().request_options(instance, request_options, options)

        props = getattr(instance, self.options["props_key"])
        token = props[self.options["token_key"]]
        if "headers" not in request_options:
            request_options["headers"] = {}
        request_options["headers"]["Authorization"] = self.options["authorization_type"] + " " + str(token)
