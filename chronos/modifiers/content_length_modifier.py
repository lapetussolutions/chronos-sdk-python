import json
from .helpers import set_header
from ..request_modifier import RequestModifier

class ContentLengthModifier(RequestModifier):
    """Request modifier to set the `Content-Length` header"""

    def request_options(self, instance, request_options, options=None):
        """Sets the `Content-Length` header to the string length of the json encoded data if the `Content-Length` header is not already set"""

        if "data" not in request_options:
            return
        data = request_options["data"]
        if type(data) is dict:
            data = json.dumps(data)

        set_header("Content-Length", str(len(data)), request_options)
