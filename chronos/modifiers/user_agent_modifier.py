from .helpers import set_header
from ..request_modifier import RequestModifier

class UserAgentModifier(RequestModifier):
    """Request modifier to set the `User-Agent` header"""

    def request_options(self, instance, request_options, options=None):
        """Sets the `User-Agent` header to the instance's `config["user_agent"]` property if the `User-Agent` header is not already set"""

        set_header("User-Agent", instance.config["user_agent"], request_options)
