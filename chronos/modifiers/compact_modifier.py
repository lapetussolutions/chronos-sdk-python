from ..request_modifier import RequestModifier

class CompactModifier(RequestModifier):
    """Request modifier to set the `compact` query string parameter"""
    
    def request_options(self, instance, request_options, options=None):
        """Adds the `compact` query string parameter if the instance's `config["compact_mode"]` is `true`"""
        
        if instance.config["compact_mode"]:
            if "params" not in request_options:
                request_options["params"] = {}
            if "compact" not in request_options["params"]:
                request_options["params"]["compact"] = "true"
