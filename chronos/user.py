import requests, os.path, json, copy, functools, time
from urllib.parse import urlencode, quote_plus
from .modifiers import auth, AuthModifier, has_header
from .request_modifier import RequestModifier
from .exceptions import RequestException, InvalidResponseFormat, UserNotCreated, UserAlreadyCreated, ImageValidationFailed, ImageAlreadyUploaded

class User:
    """Class for interacting with users within Chronos API v1.0.0

    Args:
        chronos (chronos.Chronos): Chronos instance
        props (dict, optional): User properties

            - `id` (str, optional): User ID (if the user is already created)
            - `auth_token` (str, optional): User API auth token (if the user is already created)

    Attributes:
        _chronos (chronos.Chronos): Chronos instance
        props (dict): User properties

            - `id` (str, optional): User ID
            - `auth_token` (str, optional): User API auth token
        auth ([chronos.RequestModifier, function]): Authentication request modifier
        modifiers (dict): Dictionary of default request modifiers. These will be combined with the `chronos` instance's modifiers for each request

            - `pre` (list): List of request modifiers to be applied to all requests
            - `post` (list): List of request modifiers to be applied to all requests after the `pre` list

    Examples:
        >>> from chronos import Chronos, User
        >>> chronos_instance = Chronos({})

        >>> instance = User(chronos_instance, {})
        >>> print(instance.is_created())
        False

        >>> instance = User(chronos_instance, {
        >>>     "id": "[id]",
        >>>     "auth_token": "[auth_token]",
        >>> })
        >>> print(instance.is_created())
        True

        >>> instance = chronos_instance.user({})
        >>> print(instance.is_created())
        False

        >>> instance = chronos_instance.user({
        >>>     "id": "[id]",
        >>>     "auth_token": "[auth_token]",
        >>> })
        >>> print(instance.is_created())
        True

    """

    def __init__(self, chronos, props=None):
        self._chronos = chronos

        if props is None:
            props = {}
        self.props = props

        self.set_auth_method("default", {
            "authorization_type": "token",
            "token_key": "auth_token",
        })

        def url_modifier(_, request_options, options=None):
            request_options["path"] = "/u/" + str(self.props["id"]) + "/" + request_options["path"].strip("/")

        self.modifiers = {
            "pre": [url_modifier, AuthModifier({ "instance": self })],
            "post": [],
        }

    def is_created(self):
        """Returns a bool of whether the user has the `id` property set

        Returns:
            bool: True if `id` is set in `self.props`

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({})

            >>> user = chronos_instance.user({})
            >>> print(user.is_created())
            False

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> print(user.is_created())
            True

        """
        return bool("id" in self.props and self.props["id"])

    def _is_created(should_be_created=True):
        """Function decorator to require the user to be either created or not created to use a method

        Args:
            should_be_created (bool, optional): Whether the user should be required to be created to call the wrapped function. Defaults to True

        Raises:
            chronos.exceptions.UserNotCreated: If should_be_created is True and .is_created() is False
            chronos.exceptions.UserAlreadyCreated: If should_be_created is False and .is_created() is True

        """
        def decorator(f):
            @functools.wraps(f)
            def decorated_function(self, *args, **kwargs):
                is_created = self.is_created()
                if should_be_created and not is_created:
                    raise UserNotCreated("User must be created to use this method")
                elif not should_be_created and is_created:
                    raise UserAlreadyCreated("User must not be created to use this method")
                return f(self, *args, **kwargs)
            return decorated_function
        return decorator

    def set_auth_method(self, method="default", options=None):
        """Sets the authentication request modifier

        Args:
            method ([chronos.RequestModifier, function, str]): Authentication request modifier. Defaults to "default"
            options (dict, optional): Options to be passed to the modifier. Only used if `method` is a chronos.RequestModifier or str

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({})

            >>> user = chronos_instance.user({})
            >>> user.set_auth_method("default", {
            >>>     "authorization_type": "token",
            >>>     "token_key": "auth_token",
            >>> })

        """

        if type(method) is str:
            method = auth.auth_methods[method]
        if issubclass(method, RequestModifier):
            self.auth = method(options)
        elif callable(method):
            self.auth = method
        else:
            raise TypeError("The `method` property must be a chronos.RequestModifier, function, or str")

    def normalize_request_options(request_options):
        """Makes the request options a dictionary if a str is passed. See Chronos.normalize_request_options"""
        from .chronos import Chronos
        return Chronos.normalize_request_options(request_options)

    @_is_created(False)
    def create(self, data=None, request_options=None, **kwargs):
        """Makes a request ("POST /o/[orgid]/a/[appid]/users") to create a user in Chronos

        Args:
            data (dict, optional): User data to be sent in the request
            request_options (dict, options): Request options dictionary. See Chronos.request
            **kwargs (any): Named args to pass to the request

        Raises:
            chronos.exceptions.UserAlreadyCreated: If the user is already created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({})
            >>> create_user_response = user.create({})
            >>> print(create_user_response.json())
            {'id': '[id]', 'orgid': '[orgid]', 'appid': '[appid]', 'href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]', ..., 'auth_token': '[auth_token]', ...}

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> create_user_response = user.create({})
            Traceback (most recent call last):
            ...
            chronos.exceptions.UserAlreadyCreated: User must not be created to use this method

        """

        if data is None:
            data = {}

        default_props = {
            "appid": self._chronos.props["appid"]
        }
        default_props.update(data)
        data = default_props

        if request_options is None:
            request_options = {}

        request_options.update({
            "path": "/users",
            "data": data
        })

        res = self._chronos.post(request_options, **kwargs)
        props = res.json()

        if not ("id" in props and "auth_token" in props):
            raise RequestException(res, "User creation failed")

        self.props = props
        self.created = True
        return res

    def request_options(request_options, options=None):
        """Creates full request options dictionary using the user's default modifiers (see Chronos.request_options)

        Args:
            request_options (dict): Request options dictionary
            options (dict): Options dictionary. This will also be passed to the modifiers
        """

        if options is None:
            options = {}
        modifiers = copy.deepcopy(self.modifiers)
        if "modifiers" in options:
            for key in ["pre", "post"]:
                if key in options["modifiers"]:
                    modifiers[key].extend(options["modifiers"][key])
        options["modifiers"] = modifiers
        return self._chronos.request_options(request_options, options)

    @_is_created()
    def request(self, request_options, options=None, **kwargs):
        """Makes a request relative to the user's href and applies modifiers in `self.modifiers`

        Args:
            request_options (dict): Request options dictionary. See Chronos.request
            options (dict, optional): Options dictionary to be passed to the request
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> get_user_response = user.request({
            >>>     "method": "get"
            >>>     "path": "/"
            >>> })
            >>> print(get_user_response)
            <Response [200]>
            >>> print(get_user_response.json())
            {'id': '[id]', ..., 'created': ...}

        """

        request_options = User.normalize_request_options(request_options)

        if options is None:
            options = {}

        modifiers = copy.deepcopy(self.modifiers)
        if "modifiers" in options:
            for key in ["pre", "post"]:
                if key in options["modifiers"]:
                    modifiers[key].extend(options["modifiers"][key])
        options["modifiers"] = modifiers

        return self._chronos.request(request_options, options=options, **kwargs)

    @_is_created()
    def get(self, request_options, **kwargs):
        """Makes a request using the GET method. See `request` method for more details and examples

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> get_user_response = user.get({
            >>>     "path": "/"
            >>> })
            >>> print(get_user_response)
            <Response [200]>
            >>> print(get_user_response.json())
            {'id': '[id]', ..., 'created': ...}

        """

        request_options = User.normalize_request_options(request_options)
        request_options["method"] = "get"
        return self.request(request_options, **kwargs)

    @_is_created()
    def post(self, request_options, **kwargs):
        """Makes a request using the POST method. See `request` method for more details and examples

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> post_user_response = user.post({
            >>>     "path": "/some-path"
            >>>     "data": {}
            >>> })
            >>> print(post_user_response)
            <Response [200]>
            >>> print(post_user_response.json())
            {}

        """

        request_options = User.normalize_request_options(request_options)
        request_options["method"] = "post"
        return self.request(request_options, **kwargs)

    @_is_created()
    def put(self, request_options, **kwargs):
        """Makes a request using the PUT method. See `request` method for more details and examples

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> update_user_response = user.put({
            >>>     "path": "/"
            >>>     "data": {
            >>>         "dob": "19900101"
            >>>     }
            >>> })
            >>> print(update_user_response)
            <Response [200]>
            >>> print(update_user_response.json())
            {}

        """

        request_options = User.normalize_request_options(request_options)
        request_options["method"] = "put"
        return self.request(request_options, **kwargs)

    @_is_created()
    def delete(self, request_options, **kwargs):
        """Makes a request using the DELETE method. See `request` method for more details and examples

        Args:
            request_options (dict): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> delete_response = user.delete({
            >>>     "path": "/some/path"
            >>> })
            >>> print(delete_response)
            <Response [200]>
            >>> print(delete_response.json())
            {}

        """

        request_options = User.normalize_request_options(request_options)
        request_options["method"] = "delete"
        return self.request(request_options, **kwargs)

    @_is_created()
    def get_user(self, request_options=None, **kwargs):
        """Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]") to retrieve the user's base data

        Args:
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> get_user_response = user.get_user({})
            >>> print(get_user_response)
            <Response [200]>
            >>> print(get_user_response.json())
            {'id': '[id]', ..., 'created': '2018-04-06T17:10:33+0000', ...}

        """

        if request_options is None:
            request_options = {}

        request_options["path"] = "/"
        return self.get(request_options, **kwargs)

    @_is_created()
    def update_user(self, data, request_options=None, **kwargs):
        """Makes a request ("PUT /o/[orgid]/a/[appid]/u/[userid]") to update a user's base data

        Args:
            data (dict): Data to update the user with
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> update_user_response = user.update_user({
            >>>     "dob": "19900101"
            >>> })
            >>> print(update_user_response)
            <Response [200]>
            >>> print(update_user_response.json())
            {'status': ..., 'code': ..., 'message': '', 'developerMessage': '', 'details': None}

        """

        if not len(data):
            raise Exception("The data parameter must not be empty")
        if request_options is None:
            request_options = {}

        request_options["path"] = "/"
        request_options["data"] = data
        return self.put(request_options, **kwargs)

    @_is_created()
    def get_meta(self, request_options=None, **kwargs):
        """Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/meta") to retrieve a user's meta object

        Args:
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> get_meta_response = user.get_meta({})
            >>> print(get_meta_response)
            <Response [200]>
            >>> print(get_meta_response.json())
            {'id': '[id]', 'href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/meta', 'meta': {...}}

        """

        if request_options is None:
            request_options = {}

        request_options["path"] = "/meta"
        return self.get(request_options, **kwargs)

    @_is_created()
    def update_meta(self, data, request_options=None, **kwargs):
        """Makes a request ("PUT /o/[orgid]/a/[appid]/u/[userid]/meta") to update a user's meta object

        Args:
            data (dict): Data to update the user's meta with
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })
            >>> update_meta_response = user.update_meta({
            >>>     "some_custom_field": "some_custom_val"
            >>>     "another_custom_field": "another_custom_val"
            >>> })
            >>> print(update_meta_response)
            <Response [200]>
            >>> print(update_meta_response.json())
            {}

        """

        if not len(data):
            raise Exception("The data parameter must not be empty")
        if request_options is None:
            request_options = {}

        request_options["path"] = "/meta"
        request_options["data"] = data
        return self.put(request_options, **kwargs)

    @_is_created()
    def delete_user(self, request_options=None, **kwargs):
        """Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]") to delete the user

        Args:
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        """

        if request_options is None:
            request_options = {}

        if "path" not in request_options:
            request_options["path"] = "/"

        if not has_header("Accept", request_options):
            if "headers" not in request_options:
                request_options["headers"] = {}
            request_options["headers"]["Accept"] = "text/html"

        return self.delete(request_options, **kwargs)

    image_errors = {
        "UNKNOWN":        "The upload failed for an unknown reason",
        "NO_FACE":        "We were unable to find a face in the image.",
        "MULTIPLE_FACES": "More than one face was found in the image. Please take a picture where you are the only subject!",
        "LOW_RESOLUTION": "Image is too small.",
        "BAD_FOCUS":      "The image may be too blurry to process. Please try using the auto focus feature of your camera.",
        "BAD_CONTRAST":   "The image may be too dark or too bright to process. Please try again in an area with better lighting.",
        "OPEN_MOUTH":     "You might be smiling or your mouth may be open. Please try again with a neutral expression and a closed mouth.",
        "CLOSED_EYE":     "One or both of your eyes may be closed. Please try again.",
        "GLASSES":        "You might be wearing glasses. Please try again after removing your glasses.",
        "BAD_POSE":       "You may not be looking directly at the camera. Please try again facing directly into the camera.",
        "CELEBRITY":      "Are you really %s? Make sure you are uploading a selfie of YOU!",
    }

    def get_image_error_language(key, data = []):
        """Returns the language of an image error

        Args:
            key (string): Key of the image error
            data (dict): Image upload response dictionary

        Returns:
            string: Error language

        """
        key = key.upper()
        if key == "CELEBRITY":
            names = []
            try:
                for celebrity in data["details"]["celebrities"]:
                    if type(celebrity) is dict and "name" in celebrity:
                        names.append(celebrity["name"])
            except:
                pass
            if not len(names):
                names.append("a celebrity")
            return User.image_errors[key] % ", ".join(names)
        return User.image_errors[key]

    def get_image_errors(data, language = False):
        """Creates a formatted list of errors from an image upload response dictionary

        Args:
            data (dict): Dictionary of image upload response data
            language (bool): If `False`, the error keys will be returned. If `True` is returned, the language for each error key will be returned (see `image_errors` dict above)

        Returns:
            list: List of formatted errors

        Examples:
            >>> from chronos import User

            >>> print(User.get_image_errors("", language=True))
            ["The upload failed for an unknown reason"]

            >>> print(User.get_image_errors({}, language=True))
            ["The upload failed for an unknown reason"]

            >>> print(User.get_image_errors({ "details": {} }, language=True))
            ["We were unable to find a face in the image."]

            >>> print(User.get_image_errors({ "details": { "face": { "faces": 2 } } }, language=True))
            ["More than one face was found in the image. Please take a picture where you are the only subject!"]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": False } } }, language=True))
            ["Image is too small."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True }, "image": {} } }, language=True))
            ["The image may be too blurry to process. Please try using the auto focus feature of your camera.", "The image may be too dark or too bright to process. Please try again in an area with better lighting."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True, "openmouth": True }, "image": { "focus": True, "contrast": True } } }, language=True))
            ["You might be smiling or your mouth may be open. Please try again with a neutral expression and a closed mouth."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True, "closedeyes": { "left": True } }, "image": { "focus": True, "contrast": True } } }, language=True))
            ["One or both of your eyes may be closed. Please try again."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True, "closedeyes": { "right": True } }, "image": { "focus": True, "contrast": True } } }, language=True))
            ["One or both of your eyes may be closed. Please try again."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True }, "FaceAttributes": [{ "Glasses": True }], "image": { "focus": True, "contrast": True } } }, language=True))
            ["You might be wearing glasses. Please try again after removing your glasses."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True, "pose": { "yaw": 50 } }, "image": { "focus": True, "contrast": True } } }, language=True))
            ["You may not be looking directly at the camera. Please try again facing directly into the camera."]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True }, "image": { "focus": True, "contrast": True }, "celebrities": { "0": { "name": "Some Celebrity" } } } }, language=True))
            ["Are you really Some Celebrity? Make sure you are uploading a selfie of YOU!"]

            >>> print(User.get_image_errors({ "details": { "face": { "resolution": True, "contrast": True }, "image": { "focus": True, "contrast": True } } }, language=True))
            ["The upload failed for an unknown reason"]

        """

        errors = []
        if type(data) is dict and "details" in data and type(data["details"]) is dict:
            details = data["details"]
            if "face" not in details or type(details["face"]) is not dict:
                key = "NO_FACE"
                if language:
                    return [User.get_image_error_language(key, data)]
                return [key]
            face = details["face"]
            if "faces" in face and type(face["faces"]) is int and face["faces"] > 1:
                key = "MULTIPLE_FACES"
                if language:
                    return [User.get_image_error_language(key, data)]
                return [key]
            if not ("resolution" in face and face["resolution"]):
                key = "LOW_RESOLUTION"
                if language:
                    return [User.get_image_error_language(key, data)]
                return [key]
            image = {}
            if "image" in details and type(image) is dict:
                image = details["image"]

            face_attributes = False
            if "FaceAttributes" in details and type(details["FaceAttributes"]) is list and len(details["FaceAttributes"]):
                face_attributes = details["FaceAttributes"][0]

            try:
                closedeyes = len(list(filter(bool, face["closedeyes"].values())))
            except:
                closedeyes = False
            try:
                bad_pose = face["pose"]["yaw"] > 45
            except:
                bad_pose = False
            try:
                celebrity = "name" in details["celebrities"][0]
            except:
                celebrity = False

            image_validations = {
                "BAD_FOCUS":    not ("focus" in image and image["focus"]),
                "BAD_CONTRAST": not ("contrast" in face and face["contrast"] and "contrast" in image and image["contrast"]),
                "OPEN_MOUTH":   "openmouth" in face and face["openmouth"],
                "CLOSED_EYE":   closedeyes,
                "GLASSES":      face_attributes and "Glasses" in face_attributes and face_attributes["Glasses"],
                "BAD_POSE":     bad_pose,
                "CELEBRITY":    celebrity
            }

            for key, failed in list(image_validations.items()):
                if failed:
                    if language:
                        errors.append(User.get_image_error_language(key, data))
                    else:
                        errors.append(key)

        if not len(errors):
            key = "UNKNOWN"
            if language:
                errors.append(User.get_image_error_language(key, data))
            else:
                errors.append(key)

        return errors

    def normalize_image_id_args(self, image_id=None, image_key="default"):
        if image_id is None and type(image_key) is str:
            try:
                image_id = self.props["images"][image_key]
            except:
                pass
        return image_id

    @_is_created()
    def upload_image(self, file=None, image_contents=None, image_key="default", request_options=None, delay = 1, **kwargs):
        """Makes a request ("POST /o/[orgid]/a/[appid]/u/[userid]/images") to upload an image to the user

        Args:
            file ([str, _io.BufferedReader], optional): Filename or file handler. This parameter is ignored if image_contents is passed
            image_contents ([str, _io.BufferedReader], optional): Raw image data
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> import os
            >>> dir_path = os.path.dirname(os.path.realpath(__file__))

            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })

            >>> upload_response = user.upload_image(file=dir_path + "/path/to/image.jpg")
            >>> print(upload_response)
            <Response [200]>
            >>> print(upload_response.json())
            {'id': '[id]', 'face_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face', 'registered_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/registered', 'estimations_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/estimations', 'aged_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/aged'}

            >>> f = open(dir_path + "/path/to/image.jpg", "rb")
            >>> upload_response = user.upload_image(file=f)
            >>> f.close()
            >>> print(upload_response)
            <Response [200]>
            >>> print(upload_response.json())
            {'id': '[id]', 'face_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face', 'registered_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/registered', 'estimations_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/estimations', 'aged_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/aged'}

            >>> f = open(dir_path + "/path/to/image.jpg", "rb")
            >>> upload_response = user.upload_image(image_contents=f.read())
            >>> f.close()
            >>> print(upload_response)
            <Response [200]>
            >>> print(upload_response.json())
            {'id': '[id]', 'face_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face', 'registered_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/registered', 'estimations_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/estimations', 'aged_href': 'https://chronos.lapetussolutions.com/v1/o/[orgid]/a/[appid]/u/[id]/image/face/aged'}

        """

        if request_options is None:
            request_options = {}
        if "params" not in request_options:
            request_options["params"] = {}
        if "compact" not in request_options["params"]:
            request_options["params"]["compact"] = None

        if image_contents is None:
            f = None
            if callable(getattr(file, "read", None)):
                f = file
            elif not (type(file) is str and len(file) and os.path.exists(file)):
                raise Exception("File not found")
            else:
                f = open(file, "rb")
            image_contents = f.read()
            f.close()
            if not len(image_contents):
                raise Exception("File could not be read")

        request_options.update({
            "path": "/images",
            "data": image_contents
        })
        if "headers" not in request_options:
            request_options["headers"] = {}
        if "content-type" not in map(lambda key: key.lower(), request_options["headers"].keys()):
            request_options["headers"]["Content-Type"] = "image/jpeg"

        try:
            res = self.post(request_options, **kwargs)
            if type(image_key) is str:
                if "images" not in self.props:
                    self.props["images"] = {}
                self.props["images"][image_key] = res.json()['id']

            if (delay > 0):
                time.sleep(delay)

            return res
        except InvalidResponseFormat as e:
            raise e
        except RequestException as e:
            res = e.args[0]["response"]
            data = res.json()

            raise ImageValidationFailed(res, self.get_image_errors(data))

    @_is_created()
    def delete_images(self, request_options=None, **kwargs):
        """Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]/image") to delete the user's images

        Args:
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        """

        if request_options is None:
            request_options = {}

        if "path" not in request_options:
            request_options["path"] = "/images"

        if not has_header("Accept", request_options):
            if "headers" not in request_options:
                request_options["headers"] = {}
            request_options["headers"]["Accept"] = "text/html"

        return self.delete(request_options, **kwargs)


    @_is_created()
    def get_raw_image(self, path="/", image_id=None, image_key="default", width=None, request_options=None, **kwargs):
        """Retrieves the raw data of a user's image

        Args:
            path (str, optional): Path of the request. This will be appended to /images/[image_id]. Defaults to "/"
            width (int, optional): If set, the query string parameter `size=[width]` will be passed in the request
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })

            >>> raw_image_response = user.get_raw_image()
            >>> print(raw_image_response)
            <Response [200]>
            >>> print(raw_image_response.text)
            ...

        """

        image_id = self.normalize_image_id_args(image_id=image_id, image_key=image_key)

        if request_options is None:
            request_options = {}

        request_options.update({
            "path": "/images",
            "stream": True,
        })

        if type(image_id) is str:
            request_options.update({
                "path": "/images/%s" % image_id,
            })

        path = path.strip("/")
        if len(path):
            request_options["path"] += "/" + path.strip("/")
        if type(width) is int and width > 0:
            if "params" not in request_options:
                request_options["params"] = {}
            request_options["params"]["size"] = width

        if not has_header("Accept", request_options):
            if "headers" not in request_options:
                request_options["headers"] = {}
            request_options["headers"]["Accept"] = "image/jpeg"

        return self.get(request_options, **kwargs)

    @_is_created()
    def get_estimations(self, image_id=None, image_key="default", request_options=None, **kwargs):
        """Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/images/[image_id]/face/estimations") for the estimations on the user's image

        Args:
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })

            >>> get_estimations_response = user.get_estimations()
            >>> print(get_estimations_response)
            <Response [200]>
            >>> print(get_estimations_response.json())
            {'id': '[id]', 'gender': {'status': 'Complete', 'details': {'value': ...}}, 'chronage': {'status': 'Complete', 'details': {'value': ...}}, 'bmi': {'status': 'Complete', 'details': {'value': ...}}}

        """

        if request_options is None:
            request_options = {}

        request_options["path"] = "/image/face/estimations"

        image_id = self.normalize_image_id_args(image_id=image_id, image_key=image_key)
        if type(image_id) is str:
            request_options["path"] = "/images/%s/face/estimations" % image_id

        return self.get(request_options, **kwargs)

    @_is_created()
    def get_lifespan(self, request_options=None, **kwargs):
        """Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/lifespan") to get the user's lifespan table

        Args:
            request_options (dict, optional): Request options dictionary to be passed to `request`
            **kwargs (any): Named args to pass to the request

        Returns:
            requests.Response: Response from the request

        Raises:
            chronos.exceptions.UserNotCreated: The user has not been created
            chronos.exceptions.RequestException: The request failed or the response was incorrect

        Examples:
            >>> from chronos import Chronos
            >>> chronos_instance = Chronos({
            >>>     "apiroot": "https://chronos.lapetussolutions.com/v1",
            >>>     "orgid": "[orgid]",
            >>>     "appid": "[appid]",
            >>>     "apikey": "[apikey]",
            >>> })

            >>> user = chronos_instance.user({
            >>>     "id": "[id]",
            >>>     "auth_token": "[auth_token]",
            >>> })

            >>> get_lifespan_response = user.get_lifespan()
            >>> print(get_lifespan_response)
            <Response [200]>
            >>> print(get_lifespan_response.json())

        """

        if request_options is None:
            request_options = {}

        request_options["path"] = "/lifespan"
        if "params" not in request_options:
            request_options["params"] = {}
        if "compact" not in request_options["params"]:
            request_options["params"]["compact"] = None

        return self.get(request_options, **kwargs)
