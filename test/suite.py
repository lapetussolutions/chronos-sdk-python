import unittest

from .chronos_methods import TestChronosPureMethods
from .user_methods import TestUserPureMethods

def suite():
    suite = unittest.TestSuite()
    test_classes = [TestChronosPureMethods, TestUserPureMethods]
    for test_class in test_classes:
        for key, prop in dict(test_class.__dict__).items():
            if callable(prop):
                suite.addTest(test_class(key))
    return suite
