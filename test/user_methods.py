import unittest, sys, json, os, time, random, datetime
import chronos
from chronos import Chronos, User, RequestModifier, auth, modifiers, exceptions
dir_path = os.path.dirname(os.path.realpath(__file__))

props = {
    'apikey': '',
    'apiroot': '',
    'appid': '',
    'orgid': ''
}

props.update(json.loads(sys.argv[-1]))
chronos_instance = Chronos(props)

created_props = {
    'id': 'example_id',
    'auth_token': 'example_auth_token'
}
default_props = {}

def create_user(self):
    instance = User(chronos_instance)
    target_props = {
        "appid": instance._chronos.props["appid"]
    }
    target_props.update(default_props)

    instance.create(default_props)
    self.assertTrue(instance.is_created())
    return instance

instance = None
class TestUserPureMethods(unittest.TestCase):

    def create_instance(self):
        global chronos_instance, created_props

        target_props = created_props.copy()
        instance = User(chronos_instance, created_props)
        self.assertIs(type(instance), User)
        self.assertEqual(target_props, instance.props)
        self.assertEqual(instance._chronos, chronos_instance)
        self.assertTrue(instance.is_created())

        target_props = {}
        instance = User(chronos_instance, {})
        self.assertIs(type(instance), User)
        self.assertEqual(target_props, instance.props)
        self.assertEqual(instance._chronos, chronos_instance)
        self.assertFalse(instance.is_created())

    def auth_methods(self):
        auth_options = {
            "some_key": "some_val",
            "another_key": "another_val"
        }
        class AuthNewModifier(RequestModifier):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
            def request_options(self, instance, request_options, options=None):
                pass
        auth.auth_methods["new"] = AuthNewModifier

        instance = User(chronos_instance, props={})
        instance.set_auth_method("default", auth_options)
        self.assertEqual(type(instance.auth), auth.auth_methods["default"])
        self.assertDictContainsSubset(auth_options, instance.auth.options)

        instance = User(chronos_instance, props={})
        instance.set_auth_method("new", auth_options)
        self.assertEqual(type(instance.auth), auth.auth_methods["new"])
        self.assertDictContainsSubset(auth_options, instance.auth.options)

    def normalize_request_options(self):
        path = "/some/path"
        self.assertDictContainsSubset({ "path": path }, User.normalize_request_options(path))

    def get_image_errors(self):
        image_errors = {
            "UNKNOWN": "The upload failed for an unknown reason",
            "NO_FACE": "We were unable to find a face in the image.",
            "MULTIPLE_FACES": "More than one face was found in the image. Please take a picture where you are the only subject!",
            "LOW_RESOLUTION": "Image is too small.",
            "BAD_FOCUS": "The image may be too blurry to process. Please try using the auto focus feature of your camera.",
            "BAD_CONTRAST": "The image may be too dark or too bright to process. Please try again in an area with better lighting.",
            "OPEN_MOUTH": "You might be smiling or your mouth may be open. Please try again with a neutral expression and a closed mouth.",
            "CLOSED_EYE": "One or both of your eyes may be closed. Please try again.",
            "GLASSES": "You might be wearing glasses. Please try again after removing your glasses.",
            "BAD_POSE": "You may not be looking directly at the camera. Please try again facing directly into the camera.",
            "CELEBRITY": "Are you really Some Celebrity? Make sure you are uploading a selfie of YOU!",
        }
        correct_face = { "resolution": True, "contrast": True }
        correct_image = { "focus": True, "contrast": True }
        tests = [
            [
                "",
                ["UNKNOWN"]
            ],
            [
                {},
                ["UNKNOWN"]
            ],
            [
                { "details": {} },
                ["NO_FACE"]
            ],
            [
                { "details": { "face": { "faces": 2 } } },
                ["MULTIPLE_FACES"]
            ],
            [
                { "details": { "face": { "resolution": False } } },
                ["LOW_RESOLUTION"]
            ],
            [
                { "details": { "face": { "resolution": True }, "image": {} } },
                ["BAD_FOCUS", "BAD_CONTRAST"]
            ],
            [
                { "details": { "face": { **correct_face, "openmouth": True }, "image": { **correct_image } } },
                ["OPEN_MOUTH"]
            ],
            [
                { "details": { "face": { **correct_face, "closedeyes": { "left": True } }, "image": { **correct_image } } },
                ["CLOSED_EYE"]
            ],
            [
                { "details": { "face": { **correct_face, "closedeyes": { "right": True } }, "image": { **correct_image } } },
                ["CLOSED_EYE"]
            ],
            [
                { "details": { "face": { **correct_face }, "FaceAttributes": [{ "Glasses": True }], "image": { **correct_image } } },
                ["GLASSES"]
            ],
            [
                { "details": { "face": { **correct_face, "pose": { "yaw": 50 } }, "image": { **correct_image } } },
                ["BAD_POSE"]
            ],
            [
                { "details": { "face": { **correct_face }, "image": { **correct_image }, "celebrities": [ { "name": "Some Celebrity" } ] } },
                ["CELEBRITY"]
            ],
            [
                { "details": { "face": { **correct_face }, "image": { **correct_image } } },
                ["UNKNOWN"]
            ],
        ]

        instance = User(chronos_instance)
        for test in tests:
            data, error_keys = test
            self.assertEqual(error_keys, User.get_image_errors(data))

            errors = []
            for key in error_keys:
                errors.append(image_errors[key])

            self.assertEqual(errors, User.get_image_errors(data, True))

    # everything after this point will make requests to the apiroot endpoint you pass in the config

    def create(self):
        global chronos_instance, created_props, default_props
        instance = User(chronos_instance, created_props)
        with self.assertRaises(chronos.exceptions.UserAlreadyCreated):
            instance.create()
        self.assertTrue(instance.is_created())
        instance = create_user(self)
        self.assertTrue(instance.is_created())

    def get_user(self):
        instance = create_user(self)
        data = instance.get_user()

    def chronos_create_user(self):
        global chronos_instance, default_props
        data = {
            "gender": "male" if random.random() > 0.5 else "female",
            "height": random.randint(48, 78),
            "weight": random.randint(80, 200),
        }
        instance = chronos_instance.create_user({})
        instance = chronos_instance.create_user(data)
        self.assertTrue(instance.is_created())
        self.assertDictContainsSubset(data, instance.get_user().json())

    def update_user(self):
        instance = create_user(self)
        update_props = {
            "gender": "male" if random.random() > 0.5 else "female",
            "height": random.randint(48, 78),
            "weight": random.randint(80, 200),
        }
        instance.update_user(update_props)
        data = instance.get_user().json()
        self.assertDictContainsSubset(update_props, data)

    def upload_image(self):
        def check(*args, **kwargs):
            instance = create_user(self)
            instance.upload_image(*args, **kwargs)
            return instance

        check(props["images"]["correct"])

        f = open(props["images"]["correct"], "rb")
        check(f)
        f.close()

        f = open(props["images"]["correct"], "rb")
        check(image_contents=f.read())
        f.close()

        with self.assertRaises(chronos.exceptions.ImageValidationFailed):
            check(props["images"]["incorrect"])

    def get_raw_image_deprecated_path(self):
        instance = create_user(self)
        instance.upload_image(props["images"]["correct"], image_key=None)

        res = instance.get_raw_image(image_key=None)
        self.assertGreater(len(res.text), 0, "Raw image output length is > 0")

    def get_raw_image(self):
        instance = create_user(self)
        instance.upload_image(props["images"]["correct"])

        res = instance.get_raw_image()
        self.assertGreater(len(res.text), 0, "Raw image output length is > 0")

        # works with multiple images on same instance
        instance.upload_image(props["images"]["correct"], image_key="second")

        res = instance.get_raw_image(image_key="second")
        self.assertGreater(len(res.text), 0, "Raw image output length is > 0")

        # works with manually provided IDs
        first_image_id = instance.props["images"]["default"]
        second_image_id = instance.props["images"]["second"]
        instance = chronos_instance.user({
            **instance.props,
            "images": {},
        })

        res = instance.get_raw_image(image_id=first_image_id)
        self.assertGreater(len(res.text), 0, "Raw image output length is > 0")
        res = instance.get_raw_image(image_id=second_image_id)
        self.assertGreater(len(res.text), 0, "Raw image output length is > 0")

    def delete_images(self):
        instance = create_user(self)
        instance.upload_image(props["images"]["correct"])

        instance.delete_images()
        with self.assertRaises(chronos.exceptions.RequestException):
            instance.get_raw_image()

    def delete_user(self):
        instance = create_user(self)
        instance.upload_image(props["images"]["correct"])

        instance.delete_user()
        with self.assertRaises(chronos.exceptions.RequestException):
            instance.get_raw_image()

    def estimations(self):
        instance = create_user(self)
        instance.upload_image(props["images"]["correct"])
        estimations = {}
        complete = False
        for i in range(0, 5):
            time.sleep(2)
            estimations = instance.get_estimations().json()
            complete = True
            for key in ["gender", "chronage", "bmi"]:
                try:
                    estimations[key]["details"]["value"]
                except KeyError:
                    complete = False
            if complete:
                break
        self.assertTrue(complete)

    def lifespan(self):
        instance = create_user(self)

        current_year = datetime.date.today().year
        dob = datetime.date(random.randrange(current_year - 95, current_year - 19), random.randrange(1, 12), random.randrange(1, 28))

        instance.update_user({
            "gender": "male" if random.random() > 0.5 else "female",
            "dob": dob.strftime('%Y%m%d'),
        })

        compact_keys = ["baseline", "adjusted", "vbt_baseline"]
        full_keys = [*compact_keys, "age", "baseline_survival", "baseline_survival_error", "adjusted_survival", "adjusted_survival_error", "vbt_baseline_survival", "vbt_baseline_survival_error", "adjusted_deathrate"]

        compact_lifespan = instance.get_lifespan({
            "params": {
                "compact": True
            }
        }).json()
        full_lifespan = instance.get_lifespan().json()
        csv_lifespan = instance.get_lifespan({
            "headers": {
                "Content-Type": "text/csv"
            }
        }).text

        for key in compact_keys:
            self.assertIn(key, compact_lifespan)

        for key in full_keys:
            self.assertIn(key, full_lifespan)

    # def id(self):
    #     instance = create_user(self)

    #     if not ("id" in props["images"] and "bad" in props["images"]["id"]):
    #         return

    #     bad = props["images"]["id"]["bad"]

    #     upload_tests = [
    #         {
    #             "should_succeed": False,
    #             "images": []
    #         }
    #     ]

    #     front = ""
    #     if "front" in props["images"]["id"]:
    #         front = props["images"]["id"]["front"]
    #     back = ""
    #     if "back" in props["images"]["id"]:
    #         back = props["images"]["id"]["back"]

    #     if front:
    #         upload_tests.append({
    #             "should_succeed": True,
    #             "images": [
    #                 {
    #                     "side": "front",
    #                     "file": front,
    #                 }
    #             ]
    #         })

    #         if back:
    #             upload_tests.append({
    #                 "should_succeed": True,
    #                 "images": [
    #                     {
    #                         "side": "back",
    #                         "file": back,
    #                     }
    #                 ]
    #             })

    #     upload_tests.append({
    #         "should_succeed": False,
    #         "images": [
    #             {
    #                 "side": "back",
    #                 "file": bad,
    #             }
    #         ]
    #     })

    #     if back:
    #         upload_tests.append({
    #             "should_succeed": False,
    #             "images": [
    #                 {
    #                     "side": "front",
    #                     "file": bad,
    #                 },
    #                 {
    #                     "side": "back",
    #                     "file": back,
    #                 }
    #             ]
    #         })

    #     for test in upload_tests:
    #         should_succeed = test["should_succeed"]
    #         images = test["images"]

    #         for image in images:
    #             side = image["side"]
    #             file = image["file"]

    #             print(side, file)
    #             instance.upload_id_image(file, side=side)
    #             instance.upload_id_image(open(file, "rb"), side=side)

    #             f = open(file, "rb")
    #             instance.upload_id_image(image_contents=f.read(), side=side)
    #             f.close()

    #         analysis = instance.get_id_image_analysis().json()

    #         if analysis["status"] == "In Progress":
    #             time.sleep(10)
    #             for i in range(10):
    #                 analysis = instance.get_id_image_analysis()

    #                 print(i, analysis.text)
    #                 analysis = analysis.json()
    #                 # print(analysis)
    #                 if analysis["status"] != "In Progress":
    #                     break
    #                 time.sleep(5)

    #         success = analysis["result"] == "Success"

    #         self.assertEqual(success, should_succeed)

    #     # instance.delete_id_images()

