import unittest, sys, json, requests
from chronos import Chronos, User, RequestModifier, auth, apply_request_modifier, apply_request_modifiers

default_props = {
    'apikey': '',
    'apiroot': '',
    'appid': '',
    'orgid': ''
}

props = json.loads(sys.argv[-1])

class TestChronosPureMethods(unittest.TestCase):
    def create_instance(self):
        global default_props

        target_props = default_props.copy()
        target_props.update(props)

        instance = Chronos(props)
        self.assertEqual(target_props, instance.props)
        self.assertIs(type(instance), Chronos)

    def auth_methods(self):
        auth_options = {
            "some_key": "some_val",
            "another_key": "another_val"
        }
        class AuthNewModifier(RequestModifier):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
            def request_options(self, instance, request_options, options=None):
                pass
        auth.auth_methods["new"] = AuthNewModifier

        instance = Chronos(props)
        self.assertEqual(type(instance.auth), auth.auth_methods["default"])
        instance.set_auth_method("default", auth_options)
        self.assertDictContainsSubset(auth_options, instance.auth.options)

        instance = Chronos(props)
        instance.set_auth_method("new", auth_options)
        self.assertEqual(type(instance.auth), auth.auth_methods["new"])
        self.assertDictContainsSubset(auth_options, instance.auth.options)


    def modifiers(self):
        additional_headers = {
            "Some-Header": "Some-Value",
            "Another-Header": "Another-Value",
        }

        class SetAdditionalHeadersModifier(RequestModifier):
            def __init__(self, *args, **kargs):
                super().__init__(*args, **kargs)

            def request_options(self, instance, request_options, options=None):
                super().request_options(instance, request_options, options)
                if "headers" not in request_options:
                    request_options["headers"] = {}
                request_options["headers"].update(additional_headers)

        def set_additional_headers_modifier(instance, request_options, options=None):
            if "headers" not in request_options:
                request_options["headers"] = {}
            request_options["headers"].update(additional_headers)

        modifier_tries = [
            {
                "pre": [SetAdditionalHeadersModifier()]
            },
            {
                "post": [SetAdditionalHeadersModifier()]
            },
            {
                "pre": [set_additional_headers_modifier]
            },
            {
                "post": [set_additional_headers_modifier]
            },
        ]

        instance = Chronos({})
        for modifiers in modifier_tries:
            request_options = {}
            instance.request_options(request_options, {
                "modifiers": modifiers,
            })
            self.assertDictContainsSubset(additional_headers, request_options["headers"])

        for key in ["pre", "post"]:
            for modifier in [SetAdditionalHeadersModifier(), set_additional_headers_modifier]:
                instance = Chronos({})

                request_options = {}
                apply_request_modifier(modifier, instance, request_options)
                self.assertDictContainsSubset(additional_headers, request_options["headers"])

                request_options = {}
                apply_request_modifiers([modifier], instance, request_options)
                self.assertDictContainsSubset(additional_headers, request_options["headers"])

                instance = Chronos({})
                instance.modifiers[key].append(modifier)
                request_options = {}
                instance.request_options(request_options)
                self.assertDictContainsSubset(additional_headers, request_options["headers"])

    def normalize_request_options(self):
        path = "/some/path"
        self.assertDictContainsSubset({ "path": path }, Chronos.normalize_request_options(path))


    def url(self):
        instance = Chronos(props)

        self.assertEqual('/o/' + instance.props['orgid'], instance.org_href())
        self.assertEqual('/o/' + instance.props['orgid'] + '/a/' + instance.props['appid'], instance.app_href())

        base = instance.props['apiroot'].strip("/") + "/" + instance.app_href().strip("/")
        custom = 'https://custom-url'
        path = '/some/path'
        tests = [
            [
                {},
                {
                    'url': base
                }
            ],
        ]
        for url in [base, base + '/', custom, custom + '/']:
            tests.append([
                {
                    'url': url
                },
                {
                    'url': url
                },
            ])
            for path in [path, path + '/']:
                tests.append([
                    {
                        'url': url,
                        'path': path
                    },
                    {
                        'url': url.strip("/") + "/" + path.strip("/")
                    }
                ])
        for test in tests:
            request_options, target_request_options = test
            instance.url(None, request_options)
            self.assertDictEqual(request_options, target_request_options)

    def headers(self):
        instance = Chronos(props)
        base = {
            'Authorization': 'apikey ' + instance.props['apikey'],
        }
        data = {'test': 'ing'}
        base_headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Content-Length': str(len(json.dumps(data))),
            'User-Agent': 'chronos-sdk-python/1.0.0 (Python ' + '.'.join(map(str, sys.version_info[:3])) + '; requests ' + requests.__version__ + ')'
        }
        additional = {
            'Additional-Header': 'Value'
        }
        modifier_headers = {
            'Modifier-Header': 'Modifier-Value'
        }

        def modifier(instance, request_options={}, options=None):
            request_options["headers"].update(modifier_headers)
            return request_options
        instance.modifiers["pre"].append(modifier)

        tests = [
            [
                {
                    'data': data
                },
                {
                    'headers': {
                        **base,
                        **base_headers,
                        **modifier_headers,
                    }
                }
            ],
            [
                {
                    'data': data,
                    'headers': additional.copy()
                },
                {
                    'headers': {
                        **base,
                        **base_headers,
                        **modifier_headers,
                        **additional,
                    }
                }
            ],
        ]
        for test in tests:
            request_options, target_request_options = test
            instance.request_options(request_options)
            self.assertEqual(request_options["headers"], target_request_options["headers"])

    def query_string(self):
        instance = Chronos(props, config={"compact_mode": False})
        base = {}
        additional = {
            'Param': 'Value'
        }
        modifier_params = {
            'Additional-Param': 'Value'
        }

        def modifier(instance, request_options={}, options=None):
            request_options["params"].update(modifier_params)
            return request_options
        instance.modifiers["pre"].append(modifier)

        tests = [
            [
                {},
                {
                    'params': {
                        **base,
                        **modifier_params
                    }
                }
            ],
            [
                {
                    'params': additional.copy()
                },
                {
                    'params': {
                        **base,
                        **modifier_params,
                        **additional
                    }
                }
            ],
        ]
        for test in tests:
            request_options, target_request_options = test
            instance.request_options(request_options)
            self.assertDictEqual(request_options["params"], target_request_options["params"])

    def user(self):
        instance = Chronos(props)
        user = instance.user()
        self.assertIs(type(user), User)
        self.assertEqual(user._chronos, instance)
        self.assertFalse(user.is_created())

        user = instance.user({
            "id": None
        })
        self.assertFalse(user.is_created())

        user = instance.user({
            "id": "[id]"
        })
        self.assertTrue(user.is_created())
