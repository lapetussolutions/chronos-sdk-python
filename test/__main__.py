# python3 -m test -- "{\"apiroot\":\"\",\"appid\":\"\",\"orgid\":\"\",\"apikey\":\"\",\"images\":{\"correct\":\"/abs/path/to/success.jpg\",\"incorrect\":\"/abs/path/to/success.jpg\",\"id\":{\"front\":\"/abs/path/to/front.jpg\",\"back\":\"/abs/path/to/back.jpg\",\"bad\":\"/abs/path/to/bad.jpg\"}}}"

import unittest, json, sys

try:
    config = json.loads(sys.argv[-1])
except json.decoder.JSONDecodeError:
    raise Exception('Usage: python3 -m test ' + json.dumps(json.dumps({
        'apikey': '',
        'apiroot': '',
        'appid': '',
        'orgid': ''
    })))

from .suite import suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner(failfast=True)
    runner.run(suite())